# For explanations check the notebook

cd(@__DIR__)
using Pkg
Pkg.activate(@__DIR__)

using DelimitedFiles
using LightGraphs
using NetworkDynamics
using OrdinaryDiffEq
using Plots


G = readdlm(joinpath(@__DIR__, "Norm_G_DTI.txt"), ',', Float64, '\n')
g = SimpleDiGraph(G)
N = nv(g)
synaptic_weights = G + G'

# FHN parameters
const ϵ = 0.1        # time separation
const a = 0.45        # bifurcation parameter
const b = 0.9         # bifurcation parameter
const σ = .5       # coupling strength

# Speed optimization parameters
const α = 1. / 3.
const aϵ = a * ϵ
const bϵ = b * ϵ


# Weight parameters
edgep = Array{Float64}(undef, 1, ne(g))
for (i, edge) in zip(1:ne(g), collect(edges(g)))
    edgep[i] = synaptic_weights[edge.src, edge.dst]
end
edgep .*= σ # multiply by coupling strength beforehand to save computations later

@inline Base.@propagate_inbounds function fhn_stp!(dξ, ξ, edges, p, t)
    # u, v = ξ[1:2]
    # FHN model
    dξ[1] = ξ[1] - ξ[1]^3 * α - ξ[2] + .8
    dξ[2] = ξ[1] * ϵ + aϵ - bϵ * ξ[2]
    # coupling
    @inbounds for e in edges
        dξ[1] -= e[1]
    end
    nothing
end

@inline Base.@propagate_inbounds function electrical_edge!(e, v_s, v_d, p, t)
    e[1] = v_s[1] * p[1]
    nothing
end

electricaledge = StaticEdge(f! = electrical_edge!, dim = 1)
odevertex = ODEVertex(f! = fhn_stp!, dim = 2, sym = [:u, :v])

fhn_network! = network_dynamics(odevertex, electricaledge, g);

x0 = rand(2N) * 4 .- 2

p = (nothing, edgep) # network_dynamics uses tuple syntax for parameters
tspan = (0., 1000.)
prob  = ODEProblem(fhn_network!, x0, tspan, p)

@time sol = solve(prob, Tsit5(), saveat=tspan[1]:.2:tspan[end]);

#plot(sol, tspan = tspan, vars = idx_containing(fhn_network!, :u), legend=false)
#plot!(sol, tspan = tspan, vars = idx_containing(fhn_network!, :v), legend=false, color = "lightblue")

tstart = 1
plot(sol[tstart:end], vars=(1,2))
plot!(sol[tstart:end], vars=(5,6))
plot!(sol[tstart:end], vars=(33,34))

heatmap(Array(sol)[1:2:end,1:end])

anim = @animate for t in 250:4:2000
    display_time = round(.2t, digits=3)
    scatter(sol[1:2:end, t], ylims = [-2, 2], title="t=$display_time", legend=false)
end
gif(anim, fps=12)

using FFTW, Statistics

spectrum= (abs.(fft(sol[1,250:1000] .- mean(sol[1,250:1000]))).^2)
findall(x -> x == maximum(spectrum), spectrum)

#vline([-0.813, 0.813]./2, label = "T = 2.46", linewidth = 2.5, color=:red)
plot( collect(-75:0.2:75) ./ 150, abs.(fft(sol[1,250:1000] .- mean(sol[1,250:1000]))).^2, color = :darkblue, xlabel="Hz")
#xticks!([-1, -0.813, -0.5,0.,0.5,0.813,1.] ./2)

const affected_vars = collect(1:2:40)
const input_strength = 100.

# """@docs
# Wrapper for the fhn_network!. Has the same calling signature and will add the inputs when
# p[1][1] =  true. p is assumed to be a tuple ([trigger], (vertexp, edgep)). The trigger is
# wrapped in an array since tuples are immutable, but may contain Arrays with mutable
# elements.
# """
function fhn_input_network!(dx, x, p, t)
    fhn_network!(dx, x, p[2], t)
    if p[1][1]
        dx[affected_vars] .+= input_strength
    end
end;

input_network! = ODEFunction(fhn_input_network!,
                             syms = fhn_network!.syms,
                             mass_matrix = fhn_network!.mass_matrix);

function affect!(integrator)
    # a ? b : c is a fancy julia syntax for if a do b else c
    integrator.p[1][1] ? integrator.p[1][1] = false : integrator.p[1][1] = true
end;

using DiffEqCallbacks
Δt = 200. # after Δt the callback is applied
cb = PeriodicCallback(affect!, Δt, initial_affect=false)

input_p = ([false], p)
input_prob = ODEProblem(input_network!, x0, tspan, input_p)
sol_input = solve(input_prob, AutoTsit5(TRBDF2()), saveat=tspan[1]:.5:tspan[end],
                  callback= cb);

plot(sol_input, tspan = tspan, vars = idx_containing(fhn_network!, :u), legend=false, color = "darkred")
plot!(sol_input, tspan = tspan, vars = idx_containing(fhn_network!, :v), legend=false, color = "lightblue")

# display(@benchmark solve(input_prob, AutoTsit5(TRBDF2()), saveat=tspan[1]:.5:tspan[end], callback= cb))

window_length = 60
step_width = 20
uvars = idx_containing(fhn_network!, :u)

function get_covmat_list(sol, vars; window_length = 50, step_width = 10)
    covmat_list = Array{Float64,2}[]
    start_list = Float64[]
    for start = 1:step_width:(length(sol) - window_length)
        stop = start + window_length
        push!(covmat_list, cov(sol[vars, start:stop], sol[vars, start:stop]; dims=2))
        push!(start_list, start)
    end
    return zip(covmat_list, start_list)
end
covmat_list, start_list = get_covmat_list(sol, uvars)

anim2 = @animate for (covmat, start) ∈ get_covmat_list(sol_input, uvars, window_length= window_length, step_width =step_width)
    covmat = covmat
    heatmap(log.(covmat ./ maximum(abs.(covmat)) .+1), clims=(-1,1), title="t=$((start-1)/2)", c= :vikO)
end
gif(anim2, "cov_window_60_fps3.gif", fps = 3)

# include("speed_experiments_mtk.jl")
