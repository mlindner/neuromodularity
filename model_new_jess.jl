# For explanations check the notebook

cd(@__DIR__)
using Pkg
Pkg.activate(@__DIR__)

using DelimitedFiles
using LightGraphs
using NetworkDynamics
using OrdinaryDiffEq
# In this script we are not plotting anything
#using Plots


G = readdlm(joinpath(@__DIR__, "Root3_DTI.txt"), '\t', Float64, '\n')
g = SimpleDiGraph(G)
N = nv(g)
synaptic_weights = G
#synaptic_weights ./= max(synaptic_weights...)

# Extract edge weights

edge_weights = Array{Float64}(undef, 1, ne(g))
for (i, edge) in zip(1:ne(g), collect(edges(g)))
    edge_weights[i] = synaptic_weights[edge.src, edge.dst]
end

# FHN parameters
const ϵ = 0.1        # time separation
const a = 0.45        # bifurcation parameter
const b = 0.9         # bifurcation parameter
#const σ = .5       # coupling strength

# Speed optimization parameters
const α = 1. / 3.
const aϵ = a * ϵ
const bϵ = b * ϵ

# Defining network functions
begin
    @inline Base.@propagate_inbounds function fhn_stp!(dξ, ξ, edges, p, t)
        # u, v = ξ[1:2]
        # FHN model
        dξ[1] = ξ[1] - ξ[1]^3 * α - ξ[2] + .8
        dξ[2] = ξ[1] * ϵ + aϵ - bϵ * ξ[2]
        # coupling
        @inbounds for e in edges
            dξ[1] -= e[1]
        end
        nothing
    end

    @inline Base.@propagate_inbounds function electrical_edge!(e, v_s, v_d, p, t)
        e[1] = v_s[1] * p[1]
        nothing
    end

    electricaledge = StaticEdge(f! = electrical_edge!, dim = 1, coupling = :directed)
    odevertex = ODEVertex(f! = fhn_stp!, dim = 2, sym = [:u, :v])

    fhn_network! = network_dynamics(odevertex, electricaledge, g);
end

#we only need random x0 for the first step
x0 = rand(2N) * 4 .- 2

for σ in [5.,2.,1.0,0.9,0.8,0.7,0.6,0.5,0.4,0.]
#for σ in [0,0.05,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,2,5,10]
    global x0

    # Edge parameters are edge_weights multiplied with coupling strength
    edgep = σ * edge_weights

    p = (nothing, edgep) # network_dynamics uses tuple syntax for parameters
    tspan = (0., 10.)
    prob  = ODEProblem(fhn_network!, x0, tspan, p)

    @time sol = solve(prob, Tsit5(), reltol = 1e-6, saveat=tspan[1]:1:tspan[end])

    # writedlm( "/Volumes/004915257822556/sigma-outputs-model2/Root6/res1_rev_10t_u_v_$σ.csv",  sol[:,end-1000:end], ',')
    x0 = sol[:,end]
end
