
@inline function fhn_stp!(dξ, ξ, e_s, e_d, p, t)
    # unpacking variables (omitting index k)
    u, v, x, y = ξ
    du, dv, dx, dy = dξ
    # Input (implement via callback later)
    in = 1 + tanh(t - 200)  - (1 + tanh(t-220))
    # FHN model
    du = u - u^3 / 3 - v + in * 10
    dv = (u + a - b * v) * ϵ
    # STP model
    dx = (1 - x) / τᴰ - x * y * (u - u₀)
    dy = (y₀ - y) / τᶠ - y₀ * (1 - y) * (u - u₀)
    # coupling
    c = 0
    for e in e_s # edges for which vertex is source
        c += e[1]
    end
    for e in e_d
        c += e[2] # edges for which vertex is destination
    end
    du += σ * c
    # packing variables
    ξ .= u, v, x, y
    dξ .= du, dv, dx, dy
    nothing
end
