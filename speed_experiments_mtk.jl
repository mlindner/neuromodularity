### Appendix: Experiments with further speed optimizations


using ModelingToolkit

# Tracing without parameters and simplify is faster
@variables z[1:4N]
@variables dz[1:4N]
@parameters modp[1:ne(g)]
fhn_network!(dz,z, (nothing, modp), 0.)
dz
fastf = eval(ModelingToolkit.build_function(dz,z, modp)[2])

fastf! = (du,u,p,t)-> fastf(du,u,p[2])
display(@benchmark fastf!(dx0,x0,p,0.))

# Jacobian via tracing (takes long even with static parameters and N=60)
jac_trace = ModelingToolkit.jacobian(dz, z)
fjac = eval(ModelingToolkit.build_function(jac_trace,z,modp)[2])
            #parallel=ModelingToolkit.MultithreadedForm())[2])
probjac = ODEProblem(fhn_network!,
                        jac = (du,u,p,t) -> fjac(du,u,p[2]),
                        jac_prototype = similar(jac_trace,Float64), x0, tspan, p)


fastprobjac = ODEProblem(fastf!,
                        jac = (du,u,p,t) -> fjac(du,u,p[2]),
                        jac_prototype = similar(jac_trace,Float64), x0, tspan, p)

fastprob = ODEProblem(fastf!, x0, tspan, p)

# Building Jacobian via modelingtoolkitize(probably taking very long for a lage system)
sys = modelingtoolkitize(prob)
jac = eval(ModelingToolkit.generate_jacobian(sys)[2])
f! = ODEFunction(fhn_network!, jac=jac)
jac_prob = ODEProblem(f!, jac=jac, x0, tspan)

# speed gain is practically not noticeable (due to switching?)
display(@benchmark solve(prob, AutoTsit5(TRBDF2())))

# only implicit
display(@benchmark solve(prob, TRBDF2()))
display(@benchmark solve(fastprob, TRBDF2()))

#MTK vs ND accuracy test
x0 = ones(4N) * 5 .+ randn(4N)
x01 = copy(x0)
x02 = copy(x0)
fhn_network!(x01,x0,(nothing,edgep),0)
fastf!(x02,x0,(nothing,edgep),0)
x01 == x02
