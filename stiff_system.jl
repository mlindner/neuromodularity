using OrdinaryDiffEq, Plots

# FHN parameters
const ϵ = 0.1        # time separation
const a = 0.45       # bifurcation parameter
const b = 0.9        # bifurcation parameter
const σ = 0.5        # coupling strength
const I = 0.8        # excitation

# STP params
const τᴰ = 200.      # depression time
const τᶠ = 1500.     # facilitation time
const u₀ = -2.       # baseline activity
const y₀ = 0.2       # baseline utilization

function fhn!(du,u,p,t)
    # oszillator 1
    du[1] = u[1] - u[1]^3 / 3. - u[2] + I
    du[2] = ϵ * (u[1] + a - b * u[2])
    # stp
    du[3] = (1. - u[3]) / τᴰ - u[3] * u[4] * (u[1] - u₀)
    du[4] = (y₀ - u[4]) / τᶠ + y₀ * (1 - u[4]) * (u[1] - u₀)

    # oszillator 2
    du[5] = u[5] - u[5]^3 / 3. - u[6] + I
    du[6] = ϵ * (u[5] + a - b * u[6])
    # stp
    du[7] = (1. - u[7]) / τᴰ - u[7] * u[8] * (u[5] - u₀)
    du[8] = (y₀ - u[8]) / τᶠ + y₀ * (1 - u[8]) * (u[5] - u₀)

    # coupling
    du[1] +=  u[7] * u[8] * (u[5] - u[1]) *  σ
    du[5] +=  u[3] * u[4] * (u[1] - u[5]) *  σ
end

u0 = ones(8)*2 + randn(8)
tspan = (0.,500.)

prob = ODEProblem(fhn!, u0, tspan)

@time sol = solve(prob, TRBDF2(), reltol=1e-6, abstol=1e-6)

#plotlyjs()

plot(sol[1:20], vars=[7,8])